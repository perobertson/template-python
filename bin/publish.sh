#!/usr/bin/env bash
set -xeuo pipefail

function check_published(){
  if curl --output /dev/null --silent --head --fail "${1}"; then
      echo "${PROJECT_NAME} v${PROJECT_VERSION} is already published"
      exit 1
  fi
}

function sign(){
  if [[ -f "${1}.asc" ]]; then
    gpg --verify "${1}.asc" "${1}"
  else
    gpg --detach-sign --default-key "${GPG_KEY_ID}" --armor "${1}"
  fi
}

if [[ "pypi.org" = "${REPOSITORY_HOST}" ]]; then
  check_published "https://pypi.org/project/${PROJECT_NAME}/${PROJECT_VERSION}/"
elif [[ "test.pypi.org" = "${REPOSITORY_HOST}" ]]; then
  check_published "https://test.pypi.org/project/${PROJECT_NAME}/${PROJECT_VERSION}/"
fi

if [[ ! -f "${DIST_SOURCE}" ]]; then
  poetry build --format sdist
fi
if [[ ! -f "${DIST_WHL}" ]]; then
  poetry build --format wheel
fi

sign "${DIST_SOURCE}"
sign "${DIST_WHL}"

if [[ "pypi.org" = "${REPOSITORY_HOST}" ]]; then
  poetry run twine upload \
    "${DIST_SOURCE}" "${DIST_SOURCE}.asc" \
    "${DIST_WHL}" "${DIST_WHL}.asc"
elif [[ "test.pypi.org" = "${REPOSITORY_HOST}" ]]; then
  poetry run twine upload \
    --repository-url="https://test.pypi.org/legacy/" \
    "${DIST_SOURCE}" "${DIST_SOURCE}.asc" \
    "${DIST_WHL}" "${DIST_WHL}.asc"
else
  echo "ERROR: Unknown host ${REPOSITORY_HOST}. Unable to perform upload."
  exit 1
fi
