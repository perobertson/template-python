"""Tests for pyproject.toml."""
import toml

from myproject import __version__


def test_keyword_limit() -> None:
    """Poetry docs say only 5 keywords are allowed."""
    pyproject = toml.load("pyproject.toml")
    assert 5 >= len(pyproject["tool"]["poetry"].get("keywords", []))


def test_dependency_ordering() -> None:
    """Sorted dependencies makes it easier to see what is installed."""
    pyproject = toml.load("pyproject.toml")
    deps = list(pyproject["tool"]["poetry"]["dependencies"].keys())
    assert sorted(deps) == deps


def test_dev_dependency_ordering() -> None:
    """Sorted dependencies makes it easier to see what is installed."""
    pyproject = toml.load("pyproject.toml")
    deps = list(pyproject["tool"]["poetry"]["dev-dependencies"].keys())
    assert sorted(deps) == deps


def test_version() -> None:
    """Ensures the version that can be imported matches the package version."""
    pyproject = toml.load("pyproject.toml")
    assert __version__ == pyproject["tool"]["poetry"]["version"]
