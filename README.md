# MyProject

## Pre-Commit Hooks

[pre-commit](https://pre-commit.com/) hooks can be installed by running:

```bash
make install_hooks
```

## Running Tests

Linters can be invoked with:

```bash
make lint
```

Tests can be invoked with:

```bash
make test
```
