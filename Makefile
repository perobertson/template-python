POETRY_VERSION:= $(shell poetry version)
PROJECT_NAME:= $(firstword $(POETRY_VERSION))
PROJECT_VERSION:= $(lastword $(POETRY_VERSION))
DIST_SOURCE:= dist/$(PROJECT_NAME)-$(PROJECT_VERSION).tar.gz
DIST_WHL:= dist/$(subst -,_,$(PROJECT_NAME))-$(PROJECT_VERSION)-py3-none-any.whl

dist: $(DIST_SOURCE) $(DIST_WHL)

$(DIST_SOURCE):
	poetry build --format sdist

$(DIST_WHL):
	poetry build --format wheel

.PHONY: install
install:
	poetry install $(OPTS)

.PHONY: install_hooks
install_hooks: install
	poetry run pre-commit install

.PHONY: uninstall_hooks
uninstall_hooks:
	poetry run pre-commit uninstall

.PHONY: hooks
hooks: install
	poetry run pre-commit run --all-files

.PHONY: lint
lint:
	poetry run flake8 $(OPTS)
	poetry run mypy $(OPTS)
	poetry run bandit --ini .bandit -r

.PHONY: test
test:
	poetry run pytest $(OPTS)

.PHONY: publish
publish: clean install test $(DIST)
	DIST_SOURCE=$(DIST_SOURCE) \
	DIST_WHL=$(DIST_WHL) \
	PROJECT_NAME=$(PROJECT_NAME) \
	PROJECT_VERSION=$(PROJECT_VERSION) \
	./bin/publish.sh

.PHONY: publish_test
publish_test: clean install test $(DIST)
	DIST_SOURCE=$(DIST_SOURCE) \
	DIST_WHL=$(DIST_WHL) \
	PROJECT_NAME=$(PROJECT_NAME) \
	PROJECT_VERSION=$(PROJECT_VERSION) \
	REPOSITORY_HOST="test.pypi.org" \
	./bin/publish.sh

.PHONY: clean
clean:
	find . -name "*.pyc" -delete
	find . -name "__pycache__" -delete
